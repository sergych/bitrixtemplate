<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul class="sf-menu header_right">

<?
$previousLevel = 0;
foreach($arResult as $arItem):?>
<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>
	<?if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li  class="submenu  <?if ($arItem["SELECTED"]):?>selected<?endif?>"><a href="<?=$arItem["LINK"]?>" ><?=$arItem["TEXT"]?></a> 
				<ul>
		<?else:?>
			<li><a href="<?=$arItem["LINK"]?>" class="submenu <?if ($arItem["SELECTED"]):?>selected<?endif?>"><?=$arItem["TEXT"]?></a>
				<ul>
		<?endif?>

	<?else:?> 

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
					<li  class="submenu  <?if ($arItem["SELECTED"]):?>selected<?endif?>"><a href="<?=$arItem["LINK"]?>" ><?=$arItem["TEXT"]?></a></li>
			<?else:?>
					<li><a href="<?=$arItem["LINK"]?>" class="submenu <?if ($arItem["SELECTED"]):?>selected<?endif?>"><?=$arItem["TEXT"]?></a>
			<?endif?>
 

	<?endif?>
 
	
		<?$previousLevel = $arItem["DEPTH_LEVEL"];?>
	
<?endforeach?>
<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>
</ul>
<?endif?>
<?if (!empty($arResult)):?>
<div class="mobile_menu">
	<select>
<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<?if($arItem["SELECTED"]):?>
	<option value="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></option> 
	<?else:?>
		<option value="<?=$arItem["LINK"]?>" selected='selected'><?=$arItem["TEXT"]?></option>
	<?endif?>
	
<?endforeach?>
	</select>
</div>
<?endif?>

	 