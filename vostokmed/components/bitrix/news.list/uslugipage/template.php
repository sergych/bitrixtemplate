<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<ul class="gallery  ">
 
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<li  class="gallery_box">
		<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?php 
								if($arItem["DETAIL_PICTURE"]){
									$file = $arItem["DETAIL_PICTURE"];
									$sizeFile = array("width" => "225", "height" => "150");
									$resFile = CFile::ResizeImageGet($file,$sizeFile,BX_RESIZE_IMAGE_EXACT);
									
										?>
										<img src="<?php print $resFile['src'] ?>" alt="" />
								  
										<?php
								}
								else {
									?>
									<img src="<? print $APPLICATION->GetTemplatePath('static/images/samples/75x75/image_08.jpg')  ?>" alt="" />
									<?php
								} 
								?>
				 <div class="description">
			<h3> <?echo $arItem["NAME"]?> </h3>
			<h5>   </h5>
			</div>
			</a>		 
	</li>
	 
<?endforeach;?>
 
</ul>
 