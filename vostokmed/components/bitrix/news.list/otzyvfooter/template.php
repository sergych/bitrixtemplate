<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="clearfix">
	<div class="header_left">
		<h3 class="box_header">
			��������� ������
		</h3>
	</div>
	<div class="header_right">
		<a href="#" id="footer_recent_otzyv_prev" class="scrolling_list_control_left icon_small_arrow left_white"></a>
		<a href="#" id="footer_recent_otzyv_next" class="scrolling_list_control_right icon_small_arrow right_white"></a>
	</div>
							</div>

							<div class="scrolling_listotzyv_wrapper">
							<?php if(count($arResult["ITEMS"])>0){
								?>
								<ul class="scrolling_list latest_tweets">
								<?foreach($arResult["ITEMS"] as $arItem):?>
									<?
									$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
									$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
									?>
									<li class="icon_small_arrow right_white"> 
										<p><?echo TruncateText($arItem["PREVIEW_TEXT"],150);?>
											<abbr  class="timeago"><?echo $arItem["NAME"]?></abbr><a href="http://vostok-med.info/otzyvy/#<?=$arItem['ID']; ?>">���������</a>
										</p>
									</li>
									<?endforeach;?>
								</ul>
								<?php
							}
							else {
								?>
								<p>������� ���� ���</p>
								<?php
							}
							?>
								
							</div>
 
</div>
 