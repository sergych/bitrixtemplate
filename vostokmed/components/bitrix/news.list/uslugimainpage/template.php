<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h3 class="box_header">
					������
				</h3>

<ul class="accordion">
 
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<li>
						<div id="accordion-<?echo $arItem["CODE"]?>">
							<h3><?echo $arItem["NAME"]?></h3>
						</div>
						<div class="clearfix">
							<div class="item_content clearfix">
								<a class="thumb_image" href="#" title=" ">
								<?php 
								if($arItem["DETAIL_PICTURE"]){
									$file = $arItem["DETAIL_PICTURE"];
									$sizeFile = array("width" => "75", "height" => "75");
									$resFile = CFile::ResizeImageGet($file,$sizeFile,BX_RESIZE_IMAGE_EXACT);
									
										?>
										<img src="<?php print $resFile['src'] ?>" />
								  
										<?php
								}
								else {
									?>
									<img src="<? print $APPLICATION->GetTemplatePath('static/images/samples/75x75/image_08.jpg')  ?>" alt="" />
									<?php
								}
								
								
								?>
									
								</a>
								<p>
										<?echo $arItem["PREVIEW_TEXT"];?>
								</p>
							</div>
							<div class="item_footer clearfix">
								<a class="more blue icon_small_arrow margin_right_white" href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="����� ������">����� ������</a>
								<a class="more blue icon_small_arrow margin_right_white" href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="���������">���������</a>
							</div>
						</div>
					</li>
	 
<?endforeach;?>
 
</ul>
