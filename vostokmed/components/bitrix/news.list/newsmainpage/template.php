<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
	<h3 class="box_header">
				��������� �������
			</h3>
<div class="columns clearfix">
 <?php $count = 1; ?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
 <?php 
	if($count == 1){
		?>
		<ul class="blog column_left">
		<?php 
	}
		?>
 <?php 
	if($count == 3){
		?>
		<ul class="blog column_right">
		<?php 
	}
		?>
					<li class="post">
						<ul class="comment_box clearfix">
							<li class="date">
								<div class="value"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></div>
								<div class="arrow_date"></div>
							</li>
							<li class="comments_number">
								 
							</li>
						</ul>
						<div class="post_content">
							<a class="post_image" href="<?echo $arItem["DETAIL_PAGE_URL"]?>" title="<?echo $arItem["NAME"]?>">
							<?php 
								if($arItem["PREVIEW_PICTURE"]){
									$file = $arItem["PREVIEW_PICTURE"];
									$sizeFile = array("width" => "480", "height" => "300");
									$resFile = CFile::ResizeImageGet($file,$sizeFile,BX_RESIZE_IMAGE_EXACT);
									
										?>
										<img src="<?php print $resFile['src'] ?>" />
								  
										<?php
								}
								else {
									?>
									<img src="<? print $APPLICATION->GetTemplatePath('static/images/samples/480x300/image_03.jpg')  ?>" alt="" />
									<?php
								}
								
								
								?>
							 
							</a>
							<h2>
								<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" title="<?echo $arItem["NAME"]?>">
									<?echo $arItem["NAME"]?>
								</a>
							</h2>
							<p>
								<?echo $arItem["PREVIEW_TEXT"];?>
							</p> 
						</div>
					</li>
				  <?php 
	if($count == 2 or $count == 4){
		?>
		</ul>
		<?php 
	}
		?>
 
		<?php
		$count++; 
 ?>
  
<?endforeach;?>
 
</div>
<div class="show_all clearfix">
				<a class="more" href="/news/" title="��� �������">
					��� ������� &rarr;
				</a>
			</div>
		 