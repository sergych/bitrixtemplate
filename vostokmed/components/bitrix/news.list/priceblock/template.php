<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); 
if(count($arResult["ITEMS"])>0){
$sectionsArr = array();
$IBLOCK_ID = 5;
  $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID );
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
  while ($arSect = $db_list->GetNext())
   {
     if($arSect['DEPTH_LEVEL'] > 1){
		
		 $sectionsArr[$arSect['ID']]['ID'] = $arSect['ID'];  
		 $sectionsArr[$arSect['ID']]['NAME'] = $arSect['NAME'];  
		 $sectionsArr[$arSect['ID']]['PARENT'] = $arSect['IBLOCK_SECTION_ID'];  
	 }
	 else {
		
		 $sectionsArr[$arSect['ID']]['ID'] = $arSect['ID']; 
		 $sectionsArr[$arSect['ID']]['NAME'] = $arSect['NAME']; 
		 $sectionsArr[$arSect['ID']]['PARENT'] = null; 
	 }
	 
	 
   }
?>
 
 <h3 style="margin-top:40px;">����� �� ������ ������� ������� (����)</h3>
<table class="table table-bordered table-striped">
<thead>
<tr  >
		<th>������</th>
		<th>���������</th> 
	</tr>
</thead>
<tbody>
<?php $line = 1;  ?>
<?php $IBLOCK_SECTION_ID = null;  ?>
<?php $IBLOCK_SECTION_ID_PAR = null;  ?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	
	if($line ==2){
		$class="row_gray";
		$line =1;
	}
	else {
		
		$class="";
		
		$line =2;
	}
	if($sectionsArr[$arItem["IBLOCK_SECTION_ID"]]['PARENT'] !=null and $IBLOCK_SECTION_ID_PAR != $sectionsArr[$arItem["IBLOCK_SECTION_ID"]]['PARENT']){
	 
		$secnamepar = $sectionsArr[$sectionsArr[$arItem["IBLOCK_SECTION_ID"]]['PARENT']]['NAME'];
		?>
		<tr class="sectionServicesTitleParent"><td colspan=2><h3><?=$secnamepar   ?></h3></td></tr>
		<?php
		$IBLOCK_SECTION_ID_PAR = $sectionsArr[$arItem["IBLOCK_SECTION_ID"]]['PARENT'];
	 
	}
	
	if($IBLOCK_SECTION_ID != $arItem["IBLOCK_SECTION_ID"]  ){
	 
		$secname = $sectionsArr[$arItem["IBLOCK_SECTION_ID"]]['NAME'];
		?>
		<tr ><td colspan=2><h3><?=$secname   ?></h3></td></tr>
		<?php
		$IBLOCK_SECTION_ID = $arItem["IBLOCK_SECTION_ID"];
	 
	}
	
	?>
	<tr class="<?=$class;?>">
		<td> 	 <b><?echo $arItem["NAME"]?></b> <span class="qualification"><?echo $arItem["DISPLAY_PROPERTIES"]["CATEGORY"]["VALUE"]?></span></td>
		<td> <span><?echo $arItem["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"]?></span></td> 
		 
		 </tr>
	<?endforeach;?>
</tbody>

 
</table>
 <?php  } ?>