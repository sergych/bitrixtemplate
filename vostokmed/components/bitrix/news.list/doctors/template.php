<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if(count($arResult["ITEMS"])>0){  ?>
<h3>���� �����</h3>
<table class="table table-bordered table-striped">
<thead>
<tr  >
		<th> 		 </th>
		<th> �����������</th>
		<th> ���������� ������������ </th>
		<th> ����� ������</th>
		<th> </th>
	</tr>
</thead>
<tbody>
<?php $line = 1;  ?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	
	if($line ==2){
		$class="row_gray";
		$line =1;
	}
	else {
		
		$class="";
		
		$line =2;
	}
	
	?>
	<tr class="<?=$class;?>">
		<td> 	 <b><?echo $arItem["NAME"]?></b> <span class="qualification"><?echo $arItem["DISPLAY_PROPERTIES"]["CATEGORY"]["VALUE"]?></span></td>
		<td> <span><?echo $arItem["DISPLAY_PROPERTIES"]["EDUCATION"]["VALUE"]?></span></td>
		<td> <span><?echo $arItem["DISPLAY_PROPERTIES"]["CERTIFICATE"]["VALUE"]?> </td>
		<td> <?
		if(count($arItem["DISPLAY_PROPERTIES"]["SCHEDULE"]["VALUE"]) > 0){
			foreach($arItem["DISPLAY_PROPERTIES"]["SCHEDULE"]["VALUE"] as $schedule){
				?>
					<span class="schedule"><?php print $schedule; ?></span>
				<?php
			}
		}
		 ?> </td>
		<td> <a href="/contacts/?doctor=<?=$arItem["ID"]  ?>#online" class="more tiny blue icon_small_arrow margin_right_white margin_left_10">���������� �� �����</a> </td>
	</tr>
	<?endforeach;?>
</tbody>

 
</table>
<?php  } ?>