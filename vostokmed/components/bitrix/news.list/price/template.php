<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); 

$this->addExternalCss($this->GetFolder().'/style.css');
$sectionsArr = array();
$IBLOCK_ID = 5;
  $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID );
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
  while ($arSect = $db_list->GetNext())
   {
     if($arSect['DEPTH_LEVEL'] > 1){
		
		 $sectionsArr[$arSect['ID']]['ID'] = $arSect['ID'];  
		 $sectionsArr[$arSect['ID']]['NAME'] = $arSect['NAME'];  
		 $sectionsArr[$arSect['ID']]['PARENT'] = $arSect['IBLOCK_SECTION_ID'];  
	 }
	 else {
		
		 $sectionsArr[$arSect['ID']]['ID'] = $arSect['ID']; 
		 $sectionsArr[$arSect['ID']]['NAME'] = $arSect['NAME']; 
		 $sectionsArr[$arSect['ID']]['PARENT'] = null; 
	 }
	 
	 
   }
?>
 
 <div class="container-fluid ">
 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 priceBlock">
	 <div class=" row col-lg-12 col-md-12 col-sm-12 col-xs-12  " style="padding: 16px 0;font-size: 14px;border-bottom: 1px solid;">
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-6 headPriceBlock">
				<strong>������</strong>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 ">
				<strong>��������� (���.)</strong>
			</div>
	 </div>
	 <?php $line = 1;  ?>
	<?php $IBLOCK_SECTION_ID = null;  ?>
	<?php $IBLOCK_SECTION_ID_PAR = null;  ?>
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	
	if($line ==2){
		$class="row_gray";
		$line =1;
	}
	else {
		
		$class="";
		
		$line =2;
	}
	if($sectionsArr[$arItem["IBLOCK_SECTION_ID"]]['PARENT'] !=null and $IBLOCK_SECTION_ID_PAR != $sectionsArr[$arItem["IBLOCK_SECTION_ID"]]['PARENT']){
	 
		$secnamepar = $sectionsArr[$sectionsArr[$arItem["IBLOCK_SECTION_ID"]]['PARENT']]['NAME'];
		?>
		<div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h3><?=$secname   ?></h3>
			</div>
		<?php
		$IBLOCK_SECTION_ID_PAR = $sectionsArr[$arItem["IBLOCK_SECTION_ID"]]['PARENT'];
	 
	}
	
	if($IBLOCK_SECTION_ID != $arItem["IBLOCK_SECTION_ID"]  ){
	 
		$secname = $sectionsArr[$arItem["IBLOCK_SECTION_ID"]]['NAME'];
		?>
		<div class=" row col-lg-12 col-md-12 col-sm-12 col-xs-12 headingitemPriceBlock">
				<h3><?=$secname   ?></h3>
			</div>
		 
		<?php
		$IBLOCK_SECTION_ID = $arItem["IBLOCK_SECTION_ID"];
	 
	}
	
	?>
	 <div class=" row col-lg-12 col-md-12 col-sm-12 col-xs-12 <?=$class;?> itemPriceBlock">
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-7 ">
				<b><?echo $arItem["NAME"]?></b> <span class="qualification"><?echo $arItem["DISPLAY_PROPERTIES"]["CATEGORY"]["VALUE"]?></span>
		<span class="priceInfo"><?=$arItem["PREVIEW_TEXT"]; ?></span>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 ">
				 <span><?echo $arItem["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"]?></span>
			</div>
	 </div>
	<?endforeach;?>
 </div>
 </div>