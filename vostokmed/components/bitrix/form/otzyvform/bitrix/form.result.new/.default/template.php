<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<?=$arResult["FORM_NOTE"]?>
<div >
<a href="javascript:void(0)" id="addFeedback" class="more tiny dark_blue icon_small_arrow margin_right_white margin_left_10">�������� �����</a>
</div>
<script>
	$(document).ready(function(){

		<?php if(count($arResult['arrVALUES']) == 0){
			?>
	 	$('.contact_form').hide();
			$('#addFeedback').on('click',function(){
				$('.contact_form').toggle();
			}); 
			<?php
		}
		?>
		
		$('.contact_form form').on('submit',function(){
			return true;
		});
	});
</script>
 <div class="contact_form" style="width:500px">
 
<?=$arResult["FORM_HEADER"]?>

<table>
<?
if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y")
{
?>
	<tr>
		<td><?
/***********************************************************************************
					form header
***********************************************************************************/
if ($arResult["isFormTitle"])
{
?>
	<h3>���������� ������</h3>
<?
} //endif ;

	if ($arResult["isFormImage"] == "Y")
	{
	?>
	<a href="<?=$arResult["FORM_IMAGE"]["URL"]?>" target="_blank" alt="<?=GetMessage("FORM_ENLARGE")?>"><img src="<?=$arResult["FORM_IMAGE"]["URL"]?>" <?if($arResult["FORM_IMAGE"]["WIDTH"] > 300):?>width="300"<?elseif($arResult["FORM_IMAGE"]["HEIGHT"] > 200):?>height="200"<?else:?><?=$arResult["FORM_IMAGE"]["ATTR"]?><?endif;?> hspace="3" vscape="3" border="0" /></a>
	<?//=$arResult["FORM_IMAGE"]["HTML_CODE"]?>
	<?
	} //endif
	?>
 
		</td>
	</tr>
	<?
} // endif
	?>
</table> 
<?
/***********************************************************************************
						form questions
***********************************************************************************/
?>

	<?
	foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
	{
	?> 
			
			<div class="block">
			<?php if($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'text'){  ?> 
				<label><?=$arQuestion['CAPTION']; ?><span class="form-required starrequired">*</span></label>
				<input class="text_input" name="form_text_<?=$arQuestion['STRUCTURE'][0]['ID']; ?>" value="<?=$arQuestion['VALUE']; ?>" type="text">
			<?php } ?>
			</div>
			
			<div class="block">
			<?php if($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'textarea'){  ?> 
				<label><?=$arQuestion['CAPTION']; ?><span class="form-required starrequired">*</span></label> 
				<textarea class="text_input" name="form_textarea_<?=$arQuestion['STRUCTURE'][0]['ID']; ?>" ><?=$arQuestion['VALUE']; ?></textarea>
			<?php } ?>
			</div>
		  
	<?
	} //endwhile
	?>
 
<?
if($arResult["isUseCaptcha"] == "Y")
{
?>
	 
		<div class="block"> 
			<input class="text_input" type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40" />
		</div>
		<label><?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?></label>
		<div class="block"> 
			<input class="text_input"  type="text" name="captcha_word" size="30" maxlength="50" value=""  />
		</div>
		
<?
} // isUseCaptcha
?>
 
			<div class="block"> 
				<input <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" class="more tiny blue  " name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>" />
			  
	</div>
<p>
<?=$arResult["REQUIRED_SIGN"];?> - <?=GetMessage("FORM_REQUIRED_FIELDS")?>
</p>
<?=$arResult["FORM_FOOTER"]?>
<?
 
?></div>
