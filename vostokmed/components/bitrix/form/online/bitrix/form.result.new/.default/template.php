<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<?=$arResult["FORM_NOTE"]?>

 
<?=$arResult["FORM_HEADER"]?>

<table>
<?
if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y")
{
?>
	<tr>
		<td><?
/***********************************************************************************
					form header
***********************************************************************************/
if ($arResult["isFormTitle"])
{
?>
	<h3><?=$arResult["FORM_TITLE"]?></h3>
<?
} //endif ;

	if ($arResult["isFormImage"] == "Y")
	{
	?>
	<a href="<?=$arResult["FORM_IMAGE"]["URL"]?>" target="_blank" alt="<?=GetMessage("FORM_ENLARGE")?>"><img src="<?=$arResult["FORM_IMAGE"]["URL"]?>" <?if($arResult["FORM_IMAGE"]["WIDTH"] > 300):?>width="300"<?elseif($arResult["FORM_IMAGE"]["HEIGHT"] > 200):?>height="200"<?else:?><?=$arResult["FORM_IMAGE"]["ATTR"]?><?endif;?> hspace="3" vscape="3" border="0" /></a>
	<?//=$arResult["FORM_IMAGE"]["HTML_CODE"]?>
	<?
	} //endif
	?>

			<p><?=$arResult["FORM_DESCRIPTION"]?></p>
		</td>
	</tr>
	<?
} // endif
	?>
</table>
<br />
<?
/***********************************************************************************
						form questions
***********************************************************************************/
?>

	<?
	foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
	{
	?> 
			
		
			<?php if($arQuestion['STRUCTURE'][0]['ID'] == 4 or $arQuestion['STRUCTURE'][0]['ID'] == 3 ){  ?> 
			 
			
				<div class="block">
				<label><?=$arQuestion['CAPTION']; ?><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?></label>
				<input class="text_input" name="form_text_<?=$arQuestion['STRUCTURE'][0]['ID']; ?>" value="<?=$arQuestion['VALUE']; ?>" type="text">
				</div>
		 
			<?php } ?>
		
			
			
			<?php if($arQuestion['STRUCTURE'][0]['ID'] == 6){  ?> 
			<?php 
			CModule::IncludeModule('iblock');
			$arSelect = Array("ID", "NAME" , "CODE" ,"PROPERTY_SPECIALTY","PROPERTY_CATEGORY","PROPERTY_CERTIFICATE","PROPERTY_SCHEDULE");
			$arFilter = Array("IBLOCK_ID"=>4, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
		
				
			
			?>
			<ul class="clearfix tabs_box_navigation sf-menu">
				<li class="tabs_box_navigation_selected submenu wide"> 
				<input class="text_input" name="form_text_<?=$arQuestion['STRUCTURE'][0]['ID']; ?>" value="<?=$arQuestion['VALUE']; ?>" type="hidden"> 
					<span>�������� �����</span>
					<ul class="tabs_box_navigation_hidden">
					<?php 
					$doctor = null;
					if(isset($_GET['doctor'])){
						$doctor = $_GET['doctor'];
					}
					
						while($ob = $res->GetNextElement())
							{
							 $arFields = $ob->GetFields();
							 if($doctor && $arFields["ID"] == $doctor){
									?>
									<script>
									$(document).ready(function(){
										$('[name=form_text_<?=$arQuestion['STRUCTURE'][0]['ID']; ?>]').val("<?echo $arFields["NAME"]?>");
										$('[name=form_text_<?=$arQuestion['STRUCTURE'][0]['ID']; ?>]').next("span").html("<?echo $arFields["NAME"]?> (<?echo $arFields["PROPERTY_CATEGORY_VALUE"]?>)");
									});
									</script>
									<?php
								}
							 ?>
								<li <?php if( $doctor && $arFields["ID"] == $doctor) print "class='selected'"; ?>>
									<a href="#<?echo $arFields["CODE"]?>" title="<?echo $arFields["NAME"]?>"> <?echo $arFields["NAME"]?> <strong>(<?echo $arFields["PROPERTY_CATEGORY_VALUE"]?>)</strong></a>
								</li>
							 <?php 
							}
					?>
					</ul>
				</li>
			</ul> 
			<?php } ?>
		
		  
	<?
	} //endwhile
	?>
 
<?
if($arResult["isUseCaptcha"] == "Y")
{
?>
		<div class="block"> 
			<input class="text_input" type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40" />
		</div>
		<label><?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?></label>
		<div class="block"> 
			<input class="text_input"  type="text" name="captcha_word" size="30" maxlength="50" value=""  />
		</div>
<?
} // isUseCaptcha
?>
			<div class="block"> 
				<input <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" class="more tiny blue  " name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>" />
			  
	</div>
<p>
<?=$arResult["REQUIRED_SIGN"];?> - <?=GetMessage("FORM_REQUIRED_FIELDS")?>
</p>
<?=$arResult["FORM_FOOTER"]?>
 
 