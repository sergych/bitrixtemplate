<!DOCTYPE html>
<html>
	<head> 
		<!--meta-->
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="yandex-verification" content="6ac661757d808e46" />
		<!--style-->
		<link href="https://fonts.googleapis.com/css?family=PT+Sans|Volkhov:400i&amp;subset=cyrillic" rel="stylesheet"> 
		<?$APPLICATION->SetAdditionalCSS($APPLICATION->GetTemplatePath('static/css/reset.css'))?>
		<?$APPLICATION->SetAdditionalCSS($APPLICATION->GetTemplatePath('static/css/superfish.css'))?>
		<?$APPLICATION->SetAdditionalCSS($APPLICATION->GetTemplatePath('static/css/jquery.fancybox.css'))?>
		<?$APPLICATION->SetAdditionalCSS($APPLICATION->GetTemplatePath('static/css/jquery.qtip.css'))?>
		<?$APPLICATION->SetAdditionalCSS($APPLICATION->GetTemplatePath('static/css/jquery-ui-1.9.2.custom.css'))?>
		<?$APPLICATION->SetAdditionalCSS($APPLICATION->GetTemplatePath('static/css/style.css'))?>
		<?$APPLICATION->SetAdditionalCSS($APPLICATION->GetTemplatePath('static/css/responsive.css'))?>
		<?$APPLICATION->SetAdditionalCSS($APPLICATION->GetTemplatePath('static/css/animations.css'))?> 
		 
		<link rel="shortcut icon" href="images/favicon.ico" />
		<!--js-->
		
		<?$APPLICATION->AddHeadScript($APPLICATION->GetTemplatePath('static/js/jquery-1.11.0.min.js'))?> 
		<?$APPLICATION->AddHeadScript($APPLICATION->GetTemplatePath('static/js/jquery-migrate-1.2.1.min.js'))?> 
		<?$APPLICATION->AddHeadScript($APPLICATION->GetTemplatePath('static/js/jquery.ba-bbq.min.js'))?> 
		<?$APPLICATION->AddHeadScript($APPLICATION->GetTemplatePath('static/js/jquery-ui-1.9.2.custom.min.js'))?> 
		<?$APPLICATION->AddHeadScript($APPLICATION->GetTemplatePath('static/js/jquery.easing.1.3.js'))?> 
		<?$APPLICATION->AddHeadScript($APPLICATION->GetTemplatePath('static/js/jquery.carouFredSel-5.6.4-packed.js'))?> 
		<?$APPLICATION->AddHeadScript($APPLICATION->GetTemplatePath('static/js/jquery.sliderControl.js'))?> 
		<?$APPLICATION->AddHeadScript($APPLICATION->GetTemplatePath('static/js/jquery.timeago.js'))?> 
		<?$APPLICATION->AddHeadScript($APPLICATION->GetTemplatePath('static/js/jquery.hint.js'))?> 
		<?$APPLICATION->AddHeadScript($APPLICATION->GetTemplatePath('static/js/jquery.isotope.min.js'))?> 
		<?$APPLICATION->AddHeadScript($APPLICATION->GetTemplatePath('static/js/jquery.isotope.masonry.js'))?> 
		<?$APPLICATION->AddHeadScript($APPLICATION->GetTemplatePath('static/js/jquery.fancybox-1.3.4.pack.js'))?> 
		<?$APPLICATION->AddHeadScript($APPLICATION->GetTemplatePath('static/js/jquery.qtip.min.js'))?> 
		<?$APPLICATION->AddHeadScript($APPLICATION->GetTemplatePath('static/js/jquery.blockUI.js'))?> 
		<?$APPLICATION->AddHeadScript($APPLICATION->GetTemplatePath('static/js/main.js'))?> 
		<?$APPLICATION->AddHeadScript($APPLICATION->GetTemplatePath('static/js/jquery.timeago.ru.js'))?>

		<?$APPLICATION->ShowHead()?>
		<title><?$APPLICATION->ShowTitle()?></title>
	</head>
	<body> <div id="panel"><?$APPLICATION->ShowPanel();?></div>
		<?php
	 
     $CurrPage = $APPLICATION->GetCurPage(true);
     $HomePage = "/index.php";
	 
	 $curPageArr = explode("/",$CurrPage);
  
			    if($CurrPage != $HomePage) {
			 
				 $mainPage = false;
			 
				}
				else {
				$mainPage = true;
				} 
			   ?> 
	<div class="site_container">
			<div class="header_container">
				<div class="header clearfix">
					<div class="header_left">
						<a href="/" title="������-���">
							<img src="<? print $APPLICATION->GetTemplatePath('static/images/header_logo.png')  ?>" alt="logo" />
							<span class="logo">������-���</span>
						</a>

					</div>
					<span class="adress">��.�������� 16, �������: 45-32-00</span>
					<span class="adresstwo">�������� ����������� 4, �������: 200-150</span>
<!--menu -->
		 <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"topmenu", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "2",
		"CHILD_MENU_TYPE" => "leftfirst",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "topmenu"
	),
	false
);?>   

					</div>
			</div>
			<?php
			if($mainPage){
			?>
<!-- slider -->
<ul class="slider">
	<li style="background-image: url('<? print $APPLICATION->GetTemplatePath('static/images/slider/img1.jpg')  ?>');">
		&nbsp;
	</li>
	<li style="background-image: url('<? print $APPLICATION->GetTemplatePath('static/images/slider/img2.jpg')  ?>');">
		&nbsp;
	</li>
	<li style="background-image: url('<? print $APPLICATION->GetTemplatePath('static/images/slider/img3.jpg')  ?>');">
		&nbsp;
	</li>
</ul>
<div class="page relative noborder">
	<!--<div class="top_hint">
		Give us a call: +123 356 123 124
	</div>-->
	<!-- slider content -->
	<div class="slider_content_box clearfix">
		<div class="slider_content" style="display: block;">
			<h1 class="title">
				������<br />������
			</h1>
			<h2 class="subtitle">
				�� ������ ���������� �� �����<br />
				� ������� ��� ��� �����
			</h2>
		</div>
		<div class="slider_content">
			<h1 class="title">
				�������� �����<br />
				������� �����
			</h1>
			<h2 class="subtitle">
				�������������� ������<br />
				�� ���� ����� ��������
			</h2>
		</div>
		<div class="slider_content">
			<h1 class="title">
				� ����<br />
				�� ��������
			</h1>
			<h2 class="subtitle">
				� ��� ����������� ������������<br />
				��� �������� ����� ������� ����
			</h2>
		</div>
		<!--<div class="slider_navigation">
			<div class="slider_bar"></div>
			<a class="slider_control" id="slide_1_control" href="#" title="1">
				<span class="top_border"></span>
				<span class="slider_control_bar"></span>
				1
			</a>
			<a class="slider_control" id="slide_2_control" href="#" title="2">
				<span class="top_border"></span>
				<span class="slider_control_bar"></span>
				2
			</a>
			<a class="slider_control" id="slide_3_control" href="#" title="3">
				<span class="top_border"></span>
				<span class="slider_control_bar"></span>
				3
			</a>
		</div>-->
	</div>
	<!-- home box -->
		 <?
			$partnerLink = $APPLICATION->IncludeFile(
										$APPLICATION->GetTemplatePath("includes/homebox.php"),
										Array(),
										Array("MODE"=>"php")
									    );?>


	<div class="page_layout page_margin_top clearfix">
		<div class="page_left">
					<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"newsmainpage", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "PREVIEW_TEXT",
			1 => "PREVIEW_PICTURE",
			2 => "DETAIL_PICTURE",
			3 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "uslugi",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "4",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "�������",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "newsmainpage"
	),
	false
);?>
		
		</div>
		<div class="page_right">
			<div class="sidebar_box first">
			<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"uslugimainpage",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("PREVIEW_TEXT","DETAIL_PICTURE",""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "3",
		"IBLOCK_TYPE" => "uslugi",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "�������",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("",""),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC"
	)
);?>
				
			</div>
			<div class="sidebar_box">
				<h3 class="box_header">
					���������� �� �����
				</h3>
				<p class="info">
					���������� �� ����� ����� ����� ������� ��� ��� ��������
				</p>
				<ul class="contact_data">
					<li class="clearfix">
						<span class="social_icon phone"></span>
						<p class="value">
							�� ��������: <strong>(3852) 45-32-00</strong>
						</p>
					</li>
					<li class="clearfix">
						<span class="social_icon mail"></span>
						<p class="value">
							�� e-mail: <a href="mailto:vostock.med@yandex.ru">vostock.med@yandex.ru</a>
						</p>
					</li>
					<li class="clearfix">
						<span class="social_icon form"></span>
						<p class="value">
							��� <a href="/contacts/#online" title="������ ������">����� ������-������</a>
						</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>		
	<?php
	}
	else {
		?>
		<div class="page relative">
	<div class="page_layout left_sidebar page_margin_top clearfix">
		<div class="page_header clearfix">
			<div class="page_header_left">
				<h1 class="page_title"><?$APPLICATION->ShowTitle()?></h1>
				<?$APPLICATION->IncludeComponent(
					"bitrix:breadcrumb",
					"template_1",
					Array(
						"PATH" => "",
						"SITE_ID" => "s1",
						"START_FROM" => "0"
					)
				);?>
			
			</div>
			<div class="page_header_right">
			<?$APPLICATION->IncludeComponent(
	"bitrix:search.form", 
	"template1", 
	array(
		"PAGE" => "#SITE_DIR#search/index.php",
		"USE_SUGGEST" => "N",
		"COMPONENT_TEMPLATE" => "template1"
	),
	false
);?>
			
			</div>
		</div>
	<?php /*	<div class="page_right">
			<h3 class="box_header margin">
				������
			</h3>
						<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"categories", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "PREVIEW_TEXT",
			1 => "DETAIL_PICTURE",
			2 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "3",
		"IBLOCK_TYPE" => "uslugi",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "�������",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "categories"
	),
	false
);?>
			
			
		 
		<div class="sidebar_box">
				<h3 class="box_header">
					���������� �� �����
				</h3>
				<p class="info">
					���������� �� ����� ����� ����� ������� ��� ��� ��������
				</p>
				<ul class="contact_data">
					<li class="clearfix">
						<span class="social_icon phone"></span>
						<p class="value">
							�� ��������: <strong>(3852) 45-32-00</strong>
						</p>
					</li>
					<li class="clearfix">
						<span class="social_icon mail"></span>
						<p class="value">
							�� e-mail: <a href="mailto:vostock.med@yandex.ru">vostock.med@yandex.ru</a>
						</p>
					</li>
					<li class="clearfix">
						<span class="social_icon form"></span>
						<p class="value">
							��� <a href="#onlineFormBox" id="onlineForm" title="������ ������">����� ������-������</a>

						</p>
					</li>
				</ul>
			</div>
		</div> */ ?>
		<div class="page_left_2">
			<div class="columns clearfix">
			
		<?php
		
	}
	?>
 
